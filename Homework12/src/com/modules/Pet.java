package com.modules;

import java.util.Random;
import java.util.Set;

public abstract class Pet {
    Random random = new Random();
    protected Species species;
    protected String nickName;
    protected int age;
    protected int trickLevel = random.nextInt(100);
    protected Set<String> habits;

    public Pet() {
    }

    public Pet(String nickName, int age, int trickLevel, Set<String> habits) {
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }


    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public void eat() {
        System.out.println("All animals eat the same way");
    }

    abstract public void respond();

    @Override
    public String toString() {
        if (species == Species.Dog) {
            return "Pet{" +
//                    ", species=" + Species.Dog +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + habits +
                    '}';
        } else if (species == Species.Fish) {
            return "Pet{" +
//                    ", species=" + Species.Fish +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + habits +
                    '}';
        } else if (species == Species.DomesticCat) {
            return "Pet{" +
//                    ", species=" + Species.DomesticCat +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + habits +
                    '}';
        } else if (species == Species.RoboCat) {
            return "Pet{" +
//                    ", species=" + Species.RoboCat +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + habits +
                    '}';
        } else
            return "Pet{" +
//                    ", species=" + Species.UNKNOWN +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + habits +
                    '}';

    }

    public String prettyFormat() {
        return "species= " + species
                + ", nicknames= " + nickName
                + ", age= " + age
                + ", trickLevel= " + trickLevel
                + ", habits= " + habits;
    }
}
