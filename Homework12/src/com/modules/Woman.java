package com.modules;

import com.modules.Human;

public final class Woman extends Human {
    public Woman(String name, String surname, int iq) {
        super(name, surname, iq);
    }

    @Override
    public void greatPeople(Human human) {
        super.greatPeople(human);
    }

    public void makeup() {
        System.out.println("Makeup is done");
    }
}