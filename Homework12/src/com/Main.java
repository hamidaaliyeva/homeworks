package com;

import com.controller.FamilyController;
import com.controller.FamilyOverflowException;
import com.dao.Family;
import com.modules.Dog;
import com.modules.Fish;
import com.modules.Human;
import com.modules.Pet;

import java.util.*;
import java.util.regex.Pattern;

public class Main {
    private final static FamilyController familyController = new FamilyController();
    static Human mother = new Human("Turkan", "Qurbanli", 1990);
    static Human father = new Human("Zaur", "Qurbanli", 1988);


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean status = true;
        String menu = "";
        boolean exceptionStatus = false;
        String motherName = "";
        String motherSurname = "";
        String motherBirthYearString = "";
        String motherBirthMonthString = "";
        String motherBirthDayString = "";
        String motherIQString = "";
        String fatherName = "";
        String fatherSurname = "";
        String fatherBirthYearString = "";
        String fatherBirthMonthString = "";
        String fatherBirthDayString = "";
        String fatherIQString = "";
        while (status) {
            Human mother1 = new Human("Ayten", "Aliyeva", 1346524199000l);
            Human father1 = new Human("Nazim", "Aliyev", 1975);
            List<Human> children1 = new ArrayList<>();
            Human girl1 = new Human("Hamida", "Aliyeva", 1346524199000l, 90);
            Human boy1 = new Human("Rovshan", "Aliyev", 2000);
            Human child1 = new Human("Fidan", "Aliyeva", 2005);
            children1.add(girl1);
            children1.add(boy1);
            children1.add(child1);
            Set<String> habits = new HashSet<>();
            habits.add("sleeping");
            habits.add("eating");
            habits.add("playing");
            Pet pet1 = new Dog("Leo", 2, 9, habits);
            Pet pet2 = new Fish("Sugar", 1, 8, habits);
            Set<Pet> pets = new HashSet<>();
            pets.add(pet1);
            pets.add(pet2);
            Family family1 = new Family(mother1, father1, children1, pets);
            Human mother2 = new Human("Elvira", "Abbasova", 1990);
            Human father2 = new Human("Hamid", "Abbasov", 1986);
            List<Human> children2 = new ArrayList<>();
            Human girl2 = new Human("Ali", "Abbasov", 2006);
            Human boy2 = new Human("Saadat", "Abbasova", 2010);
            Human child2 = new Human("Inci", "Abbasova", 2015);
            Human fourthChild = new Human("Naila", "Abbasova", 2019);
            children2.add(girl2);
            children2.add(boy2);
            children2.add(child2);
            children2.add(fourthChild);
            Family family2 = new Family(mother2, father2, children2, pets);
            Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
            Human father3 = new Human("Afqan", "Djabrayilov", 1970);
            List<Human> children3 = new ArrayList<>();
            Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
            Human child3 = new Human("Sanam", "Djabrayilova", 1992);
            children3.add(boy3);
            children3.add(child3);
            Family family3 = new Family(mother3, father3, children3, pets);
            List<Family> families = familyController.getAllFamilies();
            families.add(family1);
            families.add(family2);
            families.add(family3);
            if (!exceptionStatus) {
                System.out.println("Enter the number between 1 and 9: ");
                menu = scanner.next();
            }
            switch (menu) {
                case "1":
                    Family newFamily = new Family(mother1, father2, children3, pets);
                    Family newFamily2 = new Family(mother2, father3, children1, pets);
                    Family newFamily3 = new Family(mother3, father1, children2, pets);
                    familyController.saveFamily(newFamily);
                    familyController.saveFamily(newFamily2);
                    familyController.saveFamily(newFamily3);
                    List<Family> sampleFamilyList = familyController.getAllFamilies();
                    sampleFamilyList.add(newFamily);
                    sampleFamilyList.add(newFamily2);
                    sampleFamilyList.add(newFamily3);
                    sampleFamilyList.forEach(System.out::println);
                    System.out.println("Families successfully created and saved to the database!");
                    break;
                case "2":
                    System.out.println("Display all families:");
                    familyController.displayAllFamilies();
                    break;
                case "3":
                    System.out.println("Display families more than members: ");
                    try {
                        String moreMembers = scanner.next();
                        int moreThanMembers = Integer.parseInt(moreMembers);
                        List<Family> familyGreaterThanNumber = familyController.getFamiliesBiggerThan(moreThanMembers);
                        familyGreaterThanNumber.forEach(System.out::println);
                        exceptionStatus = false;
                    } catch (NumberFormatException e) {
                        System.out.println("Please enter a number!");
                        exceptionStatus = true;
                    }
                    break;
                case "4":
                    System.out.println("Display families less than members: ");
                    try {
                        String lessMembers = scanner.next();
                        int lessThanMembers = Integer.parseInt(lessMembers);
                        List<Family> familyLessThanNumber = familyController.getFamiliesLessThan(lessThanMembers);
                        familyLessThanNumber.forEach(System.out::println);
                        exceptionStatus = false;
                    } catch (NumberFormatException e) {
                        System.out.println("Please enter a number!");
                        exceptionStatus = true;
                    }
                    break;
                case "5":
                    System.out.println("How many families with members: ");
                    try {
                        String members = scanner.next();
                        int familyMember = Integer.parseInt(members);
                        System.out.println(familyController.countFamiliesWithMemberNumber(familyMember));
                        exceptionStatus = false;
                    } catch (NumberFormatException e) {
                        System.out.println("Please enter a number!");
                        exceptionStatus = true;
                    }
                    break;
                case "6":
                    Human sampleMother = new Human();
                    if (motherName == "") {
                        System.out.println("Create a new family: ");
                        System.out.println("Enter mother's name: ");
                        motherName = scanner.next();
                    }
                    if (Pattern.matches("[a-zA-Z]+", motherName)) {
                        sampleMother.setName(motherName);
                        exceptionStatus = false;
                    } else {
                        System.out.println("Name containing only letters must be implemented!");
                        exceptionStatus = true;
                        motherName = "";
                        break;
                    }
                    if (motherSurname == "") {
                        System.out.println("Enter mother's last name: ");
                        motherSurname = scanner.next();
                    }
                    if (Pattern.matches("[a-zA-Z]+", motherSurname)) {
                        sampleMother.setSurname(motherSurname);
                        exceptionStatus = false;
                    } else {
                        System.out.println("Name containing only letters must be implemented!");
                        exceptionStatus = true;
                        motherSurname = "";
                        break;
                    }
                    try {
                        if (motherBirthYearString == "") {
                            System.out.println("Enter mother's birth year: ");
                            motherBirthYearString = scanner.next();
                        }
                        if (Integer.parseInt(motherBirthYearString) > 1000 && Integer.parseInt(motherBirthYearString) < 10000) {
                            sampleMother.setYear(Integer.parseInt(motherBirthYearString));
                            exceptionStatus = false;
                        } else {
                            System.out.println("Enter a year between 1000 and 10000");
                            exceptionStatus = true;
                            motherBirthYearString = "";
                            break;
                        }
                    } catch (NumberFormatException e) {
                        System.out.println(e);
                        System.out.println("Enter a number for year");
                        exceptionStatus = true;
                        motherBirthYearString = "";
                        break;
                    }
                    try {
                        if (motherBirthMonthString == "") {
                            System.out.println("Enter mother's month of birth: ");
                            motherBirthMonthString = scanner.next();
                        }
                        if (Integer.parseInt(motherBirthMonthString) > 0 && Integer.parseInt(motherBirthMonthString) < 13) {
                            sampleMother.setMonth(Integer.parseInt(motherBirthMonthString));
                            exceptionStatus = false;
                        } else {
                            System.out.println("Enter a year between 1 and 12");
                            exceptionStatus = true;
                            motherBirthMonthString = "";
                            break;
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Enter a number for month");
                        exceptionStatus = true;
                        motherBirthMonthString = "";
                        break;
                    }
                    try {
                        if (motherBirthDayString == "") {
                            System.out.println("Enter mother's birthday: ");
                            motherBirthDayString = scanner.next();
                        }
                        if (Integer.parseInt(motherBirthDayString) > 0 && Integer.parseInt(motherBirthDayString) < 31) {
                            sampleMother.setBirthday(Integer.parseInt(motherBirthDayString));
                            exceptionStatus = false;
                        } else {
                            System.out.println("Enter a year between 1 and 31");
                            exceptionStatus = true;
                            motherBirthDayString = "";
                            break;
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Enter a number for day");
                        exceptionStatus = true;
                        motherBirthDayString = "";
                        break;
                    }
                    try {
                        if (motherIQString == "") {
                            System.out.println("Enter mother's iq: ");
                            motherIQString = scanner.next();
                        }
                        if (Integer.parseInt(motherIQString) > 0 && Integer.parseInt(motherIQString) < 101) {
                            sampleMother.setIq(Integer.parseInt(motherIQString));
                            exceptionStatus = false;
                        } else {
                            System.out.println("Enter a year between 1 and 100");
                            exceptionStatus = true;
                            motherIQString = "";
                            break;
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Enter a number for IQ");
                        exceptionStatus = true;
                        motherIQString = "";
                        break;
                    }
                    Human sampleFather = new Human();
                    if (fatherName == "") {
                        System.out.println("Enter father's name: ");
                        fatherName = scanner.next();
                    }
                    if (Pattern.matches("[a-zA-Z]+", fatherName)) {
                        sampleFather.setName(fatherName);
                        exceptionStatus = false;
                    } else {
                        System.out.println("Name containing only letters must be implemented!");
                        exceptionStatus = true;
                        fatherName = "";
                        break;
                    }
                    if (fatherSurname == "") {
                        System.out.println("Enter father's last name: ");
                        fatherSurname = scanner.next();
                    }
                    if (Pattern.matches("[a-zA-Z]+", fatherSurname)) {
                        sampleFather.setSurname(fatherSurname);
                        exceptionStatus = false;
                    } else {
                        System.out.println("Name containing only letters must be implemented!");
                        exceptionStatus = true;
                        fatherSurname = "";
                        break;
                    }
                    try {
                        if (fatherBirthYearString == "") {
                            System.out.println("Enter father's birth year: ");
                            fatherBirthYearString = scanner.next();
                        }
                        if (Integer.parseInt(fatherBirthYearString) > 1000 && Integer.parseInt(fatherBirthYearString) < 10000) {
                            sampleFather.setYear(Integer.parseInt(fatherBirthYearString));
                            exceptionStatus = false;
                        } else {
                            System.out.println("Enter a year between 1000 and 10000");
                            exceptionStatus = true;
                            fatherBirthYearString = "";
                            break;
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Enter a number for year");
                        exceptionStatus = true;
                        fatherBirthYearString = "";
                        break;
                    }
                    try {
                        if (fatherBirthMonthString == "") {
                            System.out.println("Enter father's month of birth: ");
                            fatherBirthMonthString = scanner.next();
                        }
                        if (Integer.parseInt(fatherBirthMonthString) > 0 && Integer.parseInt(fatherBirthMonthString) < 13) {
                            sampleFather.setMonth(Integer.parseInt(fatherBirthMonthString));
                            exceptionStatus = false;
                        } else {
                            System.out.println("Enter a year between 1 and 12");
                            exceptionStatus = true;
                            fatherBirthMonthString = "";
                            break;
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Enter a number for month");
                        exceptionStatus = true;
                        fatherBirthMonthString = "";
                        break;
                    }
                    try {
                        if (fatherBirthDayString == "") {
                            System.out.println("Enter father's birthday: ");
                            fatherBirthDayString = scanner.next();
                        }
                        if (Integer.parseInt(fatherBirthDayString) > 0 && Integer.parseInt(fatherBirthDayString) < 31) {
                            sampleFather.setBirthday(Integer.parseInt(fatherBirthDayString));
                            exceptionStatus = false;
                        } else {
                            System.out.println("Enter a year between 1 and 31");
                            exceptionStatus = true;
                            fatherBirthDayString = "";
                            break;
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Enter a number for day");
                        exceptionStatus = true;
                        fatherBirthDayString = "";
                        break;
                    }
                    try {
                        if (fatherIQString == "") {
                            System.out.println("Enter father's iq: ");
                            fatherIQString = scanner.next();
                        }
                        if (Integer.parseInt(fatherIQString) > 0 && Integer.parseInt(fatherIQString) < 101) {
                            sampleFather.setIq(Integer.parseInt(fatherIQString));
                            exceptionStatus = false;
                        } else {
                            System.out.println("Enter a year between 1 and 100");
                            exceptionStatus = true;
                            fatherIQString = "";
                            break;
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Enter a number for IQ");
                        exceptionStatus = true;
                        fatherIQString = "";
                        break;
                    }
                    familyController.createNewFamily(sampleMother, sampleFather);
                    System.out.println("New family has been created!");
                    motherName = "";
                    motherSurname = "";
                    motherBirthYearString = "";
                    motherBirthMonthString = "";
                    motherBirthDayString = "";
                    motherIQString = "";
                    fatherName = "";
                    fatherSurname = "";
                    fatherBirthYearString = "";
                    fatherBirthMonthString = "";
                    fatherBirthDayString = "";
                    fatherIQString = "";
                    exceptionStatus = false;
                    break;
                case "7":
                    System.out.println("Delete family in index: ");
                    try {
                        String deleteFamilyString = scanner.next();
                        int deleteFamily = Integer.parseInt(deleteFamilyString);
                        familyController.deleteFamilyByIndex(deleteFamily);
                        familyController.displayAllFamilies();
                        exceptionStatus = false;
                    } catch (NumberFormatException e) {
                        System.out.println("Number must be implemented");
                        exceptionStatus = true;
                    }
                    break;
                case "8":
                    System.out.println("Edit the family in index: ");
                    try {
                        String editIndexString = scanner.next();
                        int editIndex = Integer.parseInt(editIndexString);
                        System.out.println("Select 1 for born child, 2 for adopt child and 3 for main menu: ");
                        String subMenuString = scanner.next();
                        int subMenu = Integer.parseInt(subMenuString);
                        if (subMenu == 1) {
                            try {
                                System.out.println("What name to give to the boy? ");
                                String boy = scanner.next();
                                System.out.println("What name to give to the girl");
                                String girl = scanner.next();
                                familyController.bornChild(families.get(editIndex), boy, girl);
                                System.out.println("Baby is born");
                                System.out.println(families.get(editIndex));
                                break;

                            } catch (FamilyOverflowException e) {
                                System.out.println(e.getMessage());
                            }
                        } else if (subMenu == 2) {
                            try {
                                System.out.println("Enter family ID: ");
                                int familyID = scanner.nextInt();
                                Human child = new Human("Inci", "Djabrayilova", 299999l);
                                familyController.adoptChild(families.get(familyID), child);
                                break;
                            } catch (FamilyOverflowException e) {
                                System.out.println(e.getMessage());
                            }
                        } else if (subMenu == 3) break;
                        exceptionStatus = false;
                    } catch (NumberFormatException e) {
                        System.out.println("Number must be implemented");
                        exceptionStatus = true;
                    }
                    break;
                case "9":
                    System.out.println("Delete children more than age: ");
                    try {
                        String ageString = scanner.next();
                        int age = Integer.parseInt(ageString);
                        familyController.deleteAllChildrenOlderThen(age);
                        familyController.displayAllFamilies();
                        exceptionStatus = false;
                    } catch (NumberFormatException e) {
                        System.out.println("Number must be implemented");
                        exceptionStatus = true;
                    }
                    break;
                case "exit":
                    status = false;
                    break;
                default:
                    System.out.println("Enter number between 1 and 9 or exit!");
                    break;
            }
        }
    }
}
