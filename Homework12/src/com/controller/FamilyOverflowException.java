package com.controller;

import com.dao.CollectionFamilyDao;
import com.dao.FamilyDao;
import com.modules.Human;
import com.service.FamilyService;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException(String message){
        super(message);
    }
}
