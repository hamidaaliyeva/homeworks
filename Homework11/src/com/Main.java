package com;

import com.controller.FamilyController;
import com.dao.Family;
import com.modules.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    private final static FamilyController familyController = new FamilyController();
    static Human mother = new Human("Turkan", "Qurbanli", 1990);
    static Human father = new Human("Zaur", "Qurbanli", 1988);


    public static void main(String[] args) {
        Human mother1 = new Human("Ayten", "Aliyeva", 1346524199000l);
//        System.out.println(mother1.describeAge());
//        System.out.println(mother1);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1346524199000l, 90);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        System.out.println("Display all families:");
        familyController.displayAllFamilies();

//        Family getByIndex = familyController.getFamilyByIndex(1);
//        System.out.println("in 1st index: " + getByIndex);

        familyController.deleteFamilyByIndex(0);
        System.out.println("After deleting family in 0th index:");
        familyController.displayAllFamilies();
//
//        familyController.deleteFamily(family2);
//        System.out.println("After deleting family2:");
//        familyController.displayAllFamilies();
//
//        familyController.saveFamily(family3);
//
//        families.add(family1);
//        families.add(family2);
//
        System.out.println("Display families more than 4 members");
        List<Family> familyGreaterThanNumber = familyController.getFamiliesBiggerThan(4);
        familyGreaterThanNumber.forEach(System.out::println);

        System.out.println("Display families less than 5 members");
        List<Family> familyLessThanNumber = familyController.getFamiliesLessThan(5);
        familyLessThanNumber.forEach(System.out::println);

        System.out.println("How many families with 7 members");
        int familyMember7 = familyController.countFamiliesWithMemberNumber(7);
        System.out.println(familyMember7);

//        familyController.createNewFamily(mother, father);
//
//        Family bornChild = familyController.bornChild(family1, "Kamandar", "Lala");
//
//        families.forEach(System.out::println);
//
        Family adoptChild = familyController.adoptChild(family2, girl1);

        System.out.println("After adoption");
        System.out.println(family2);
        families.forEach(System.out::println);

        familyController.deleteAllChildrenOlderThen(25);
        families.forEach(System.out::println);

//        int count = familyController.count();
//        System.out.println(count);
//
//        System.out.println("Get family by 0 index ");
//        System.out.println(familyController.getFamilyById(0));
//
//        System.out.println("Get Pets");
//        System.out.println(familyController.getPets(2));
//
//        Pet newPet = new DomesticCat("Micky", 3, 5, habits);
//        System.out.println("Add pet");
//        familyController.addPet(3, newPet);
//
//        families.forEach(System.out::println);


    }
}
