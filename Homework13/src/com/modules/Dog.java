package com.modules;

import java.util.Set;

public class Dog extends Pet {
    public Dog(String nickName, int age, int trickLevel, Set<String> habits) {
        super(nickName, age, trickLevel, habits);
    }

    public void respond() {
        System.out.println("Dogs often chase cats");
    }
}