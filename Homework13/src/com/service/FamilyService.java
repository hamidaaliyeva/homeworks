package com.service;

import com.controller.FamilyOverflowException;
import com.dao.CollectionFamilyDao;
import com.dao.Family;
import com.dao.FamilyDao;
import com.modules.Human;
import com.modules.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class FamilyService {

    private final FamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {

        return familyDao.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        if (index <= familyDao.getAllFamilies().size()) {
            return familyDao.getFamilyByIndex(index);
        } else return null;
    }

    public Boolean deleteFamilyByIndex(int index) {
        if (index <= familyDao.getAllFamilies().size()) {
            familyDao.getAllFamilies().remove(index);
            return true;
        } else return false;
    }

    public Boolean deleteFamily(Family family) {
        if (familyDao.getAllFamilies().contains(family)) {
            familyDao.getAllFamilies().remove(family);
            return true;
        } else return false;
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public void displayAllFamilies() {

        familyDao.displayAllFamilies();
    }


    public List<Family> getFamiliesBiggerThan(int number) {
        return familyDao.getFamiliesBiggerThan(number);
//        return familyDao.getAllFamilies().
//                stream().filter(i -> i.countFamily() > number)
//                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int number) {
        return familyDao.getFamiliesLessThan(number);
    }

    public int countFamiliesWithMemberNumber(int number) {
        return this.familyDao.countFamiliesWithMemberNumber(number);
    }

    public Family createNewFamily(Human mother, Human father) {
        return familyDao.createNewFamily(mother, father);
//        Family family = new Family();
//        family.setMother(mother);
//        family.setFather(father);
//        familyDao.saveFamily(family);
//        return family;
    }


    public Family bornChild(Family family, String masculine, String feminine) throws FamilyOverflowException {
        List<Human> children = new ArrayList<>();
        Random random = new Random();
        int gender = random.nextInt(2);
        Human child = new Human();
        if (family.countFamily() <= 5) {
            children.add(child);
            if ((family.getMother().getBirthDate() - child.getBirthDate() >= 18) &&
                    (family.getFather().getBirthDate() - child.getBirthDate() >= 18)) {
                if (gender == 0) {
                    child.setName(masculine);
                } else child.setName(feminine);
                family.setChildren(children);
            }
        } else throw new FamilyOverflowException("Enough family member!");
        return family;
    }

    public Family adoptChild(Family family, Human human) throws FamilyOverflowException {
        human = new Human(human.getName(), human.getSurname(), human.getBirthDate(), human.getIq());
        List<Human> children = new ArrayList<>();
        if (family.countFamily() <= 5) {
            children.add(human);
            family.setChildren(children);
            System.out.println(human);
        } else throw new FamilyOverflowException("Enough family member!");
        return family;
    }

    public void deleteAllChildrenOlderThen(int number) {
        familyDao.deleteAllChildrenOlderThen(number);
    }

    public int count() {
        return familyDao.count();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return familyDao.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyDao.addPet(index, pet);
    }

    public List<Family> families() {
        return familyDao.families();
    }
}
