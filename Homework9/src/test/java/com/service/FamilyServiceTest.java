package com.service;

import com.controller.FamilyController;
import com.dao.Family;
import com.modules.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class FamilyServiceTest {

    private final static FamilyController familyController = new FamilyController();

    @Test
    void getAllFamilies() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        Assertions.assertTrue(families.contains(family1) && families.contains(family2)
                && families.contains(family3));
    }

    @Test
    void getFamilyByIndex() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        Family getByIndex = familyController.getFamilyByIndex(1);
        Assertions.assertEquals(families.indexOf(getByIndex), 1);

    }

    @Test
    void deleteFamilyByIndex() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        int familySizeBefore = families.size();
        familyController.deleteFamilyByIndex(0);
        int familySizeAfter = families.size();
        Assertions.assertTrue(familySizeBefore - familySizeAfter == 1);

    }

    @Test
    void deleteFamily() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        int familySizeBefore = families.size();
        familyController.deleteFamily(family2);
        int familySizeAfter = families.size();
        Assertions.assertTrue(familySizeBefore - familySizeAfter == 1);
    }

    @Test
    void saveFamily() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        familyController.saveFamily(family3);

        Assertions.assertTrue(families.contains(family3));

    }

    @Test
    void displayAllFamilies() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        familyController.displayAllFamilies();
        int index1 = families.indexOf(family1);
        int index2 = families.indexOf(family2);
        int index3 = families.indexOf(family3);

        Assertions.assertTrue(families.contains(family1) && families.contains(family2)
                && families.contains(family3));
    }

    @Test
    void getFamiliesBiggerThan() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        List<Family> familyGreaterThanNumber = familyController.getFamiliesBiggerThan(4);
        Assertions.assertTrue(familyGreaterThanNumber.size() < families.size());
    }

    @Test
    void getFamiliesLessThan() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        List<Family> familyLessThanNumber = familyController.getFamiliesLessThan(5);
        Assertions.assertTrue(familyLessThanNumber.size() < families.size());
    }

    @Test
    void countFamiliesWithMemberNumber() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        int familyMember6 = familyController.countFamiliesWithMemberNumber(7);
        Assertions.assertTrue(familyMember6 == 0);
    }

    @Test
    void createNewFamily() {
        Human mother = new Human("Turkan", "Qurbanli", 1990);
        Human father = new Human("Zaur", "Qurbanli", 1988);
        familyController.createNewFamily(mother, father);
        Assertions.assertTrue(mother.getSurname() == "Qurbanli");

    }

    @Test
    void bornChild() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Family bornChild = familyController.bornChild(family1, "Kamandar", "Lala");
        Assertions.assertTrue(family1.getChildren().get(0).getName() == "Kamandar" ||
                family1.getChildren().get(0).getName() == "Lala");

    }

    @Test
    void adoptChild() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        Family adoptChild = familyController.adoptChild(family2, girl1);
        Assertions.assertTrue(family2.getChildren().size() == 1);
    }

    @Test
    void deleteAllChildrenOlderThen() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        familyController.deleteAllChildrenOlderThen(25);
        Assertions.assertTrue(family1.getChildren().size() == 3);
    }

    @Test
    void count() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        int count = familyController.count();
        Assertions.assertTrue(count == families.size());
    }

    @Test
    void getFamilyById() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        Family getById = familyController.getFamilyById(0);
        Assertions.assertTrue(families.indexOf(getById) == 0);
    }

    @Test
    void getPets() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        Assertions.assertTrue(familyController.getPets(1).size() == 2);

    }

    @Test
    void addPet() {
        Human mother1 = new Human("Ayten", "Aliyeva", 1980);
        Human father1 = new Human("Nazim", "Aliyev", 1975);
        List<Human> children1 = new ArrayList<>();
        Human girl1 = new Human("Hamida", "Aliyeva", 1999);
        Human boy1 = new Human("Rovshan", "Aliyev", 2000);
        Human child1 = new Human("Fidan", "Aliyeva", 2005);
        children1.add(girl1);
        children1.add(boy1);
        children1.add(child1);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
        Pet pet1 = new Dog("Leo", 2, 9, habits);
        Pet pet2 = new Fish("Sugar", 1, 8, habits);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet1);
        pets.add(pet2);
        Family family1 = new Family(mother1, father1, children1, pets);
        Human mother2 = new Human("Elvira", "Abbasova", 1990);
        Human father2 = new Human("Hamid", "Abbasov", 1986);
        List<Human> children2 = new ArrayList<>();
        Human girl2 = new Human("Ali", "Abbasov", 2006);
        Human boy2 = new Human("Saadat", "Abbasova", 2010);
        Human child2 = new Human("Inci", "Abbasova", 2015);
        Human fourthChild = new Human("Naila", "Abbasova", 2019);
        children2.add(girl2);
        children2.add(boy2);
        children2.add(child2);
        children2.add(fourthChild);
        Family family2 = new Family(mother2, father2, children2, pets);
        Human mother3 = new Human("Nazira", "Djabrayilova", 1977);
        Human father3 = new Human("Afqan", "Djabrayilov", 1970);
        List<Human> children3 = new ArrayList<>();
        Human boy3 = new Human("Orxan", "Djabrayilov", 1995);
        Human child3 = new Human("Sanam", "Djabrayilova", 1992);
        children3.add(boy3);
        children3.add(child3);
        Family family3 = new Family(mother3, father3, children3, pets);
        List<Family> families = familyController.getAllFamilies();
        families.add(family1);
        families.add(family2);
        families.add(family3);
        Pet newPet = new DomesticCat("Micky", 3, 5, habits);
        familyController.addPet(2, newPet);
        Assertions.assertTrue(pets.size() == 2);
    }
}