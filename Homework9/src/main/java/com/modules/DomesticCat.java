package com.modules;

import java.util.Set;

public class DomesticCat extends Pet {
    public DomesticCat(String nickName, int age, int trickLevel, Set<String> habits) {
        super(nickName, age, trickLevel, habits);
    }

    public void respond() {
        System.out.println("a domestic cat has 32 muscles that control the outer ear");
    }
}
