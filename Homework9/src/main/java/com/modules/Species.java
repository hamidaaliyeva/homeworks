package com.modules;

public enum Species {
    Fish,
    DomesticCat,
    Dog,
    RoboCat,
    UNKNOWN

}
