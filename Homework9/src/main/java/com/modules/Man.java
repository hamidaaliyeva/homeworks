package com.modules;

import com.modules.Human;

public final class Man extends Human {
    public Man(String name, String surname, int iq) {
        super(name, surname, iq);
    }

    @Override
    public void greatPeople(Human human) {
        super.greatPeople(human);
    }

    public void repairCar() {
        System.out.println("Car is repaired");
    }
}
