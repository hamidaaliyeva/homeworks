package com.modules;

import com.modules.Pet;

import java.util.Set;

public class RoboCat extends Pet {
    public RoboCat(String nickName, int age, int trickLevel, Set<String> habits) {
        super(nickName, age, trickLevel, habits);
    }

    public void respond() {
        System.out.println("A cat robot works with battery");
    }
}
