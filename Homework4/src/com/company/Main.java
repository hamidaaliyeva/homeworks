package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        String[] dogHabits = {"eat", "drink", "sleep"};
        Pet pet = new Pet("dog", "Rock", 5, 75, dogHabits);
        System.out.println(pet);
        Human mother = new Human("Jane", "Karleone", 1985);
        Human father = new Human("Vito", "Vito", 1975);
        Human people = new Human("Michael", "Karleone", 1977, 90, mother, father, pet);
        System.out.println(people);
        Random random = new Random();
        System.out.println(mother);
        System.out.println(father);
        Human girl = new Human("Hamida", "Aliyeva", 1999);
        System.out.println(girl);
        Human boy = new Human("Rovshen", "Aliyev", 2000, mother, father);
        System.out.println(boy);
        Pet pet1 = new Pet("Sugar");
        String[] habits = {"sleeping", "eating", "playing"};
        Pet cat = new Pet();
        int trick = cat.getTrickLevel();
        cat = new Pet("cat", "Leo", 2,trick, habits);
        Pet dog = new Pet();
        String[][] schedule = {{"weekdays"},{"go to school"},
                {"weekends"},{"relax"}};
        Human human = new Human();
        int iq = human.getIq();
        human= new Human("Fidan", "Abbaszada", 1998, iq, cat, mother, father, schedule);
        Human person = new Human();
        System.out.println(human);
        int generatedNumber = random.nextInt(100);
        human.feedPed(generatedNumber);
    }
}