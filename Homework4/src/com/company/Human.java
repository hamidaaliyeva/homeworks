package com.company;

import java.util.Arrays;
import java.util.Random;

public class Human {
    Random random = new Random();
    private String name;
    private String surname;
    private int year;
    private int iq = random.nextInt(100);
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;


    public boolean feedPed(int num){
        System.out.println("isn't it time for feeding");
        if (pet.getTrickLevel()>num) {
            System.out.print("Hm.. I will feed ");
            System.out.println(pet.getNickName());
            return true;
        }
        else {
            System.out.print("I think ");
            System.out.print(pet.getNickName());
            System.out.print(" is not hungry");
            return false;
        }
    }

    public void greatPeople(Human human){
        System.out.println("Hello, " +pet.getNickName());
    }
    public void describePet(Human human){
       if (pet.getTrickLevel()>50){
        System.out.println("I have a" +pet.getSpecies() +" he is " +pet.getAge() +
                " years old, he is very sly");}
       else System.out.println("I have a" +pet.getSpecies() +" he is " +pet.getAge() +
               " years old, he is almost not sly");
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", pet=" + pet +
                ", mother=" + mother +
                ", father=" + father +
                ", schedule=" + Arrays.toString(schedule) +
                '}';
    }


    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int year, int iq, Human mother, Human father, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.pet = pet;
    }
    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father,
                 String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public Human(){
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }


}
