package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean status = true;
        String[][] schedule = {
                {"Sunday", "do home work"},
                {"Monday", "go to courses; watch a film"},
                {"Tuesday", "go to courses; watch a film"},
                {"Wednesday", "go to courses; watch a film"},
                {"Thursday", "go to courses; watch a film"},
                {"Friday", "go to courses; watch a film"},
                {"Saturday", "do home work"}};
        String day;
        while (status) {
            String newTasks = "";
            boolean isChangeMode = false;
            System.out.println("Please, input the day of week:");
            day = scanner.nextLine();
            String[] newDay = day.trim().toLowerCase().split("\\s+");
            if (newDay[0].equals("change")) {
                isChangeMode = true;
                System.out.println("Please, input new tasks for " + newDay[1]);
                newTasks = scanner.nextLine();
            }
            switch (isChangeMode ? newDay[1] : newDay[0]) {
                case "sunday":
                    if (isChangeMode) {
                        schedule[0][1] = newTasks;
                    }
                    System.out.println("Your tasks for " + schedule[0][0] + ": " + schedule[0][1]);
                    break;
                case "monday":
                    if (isChangeMode) {
                        schedule[1][1] = newTasks;
                    }
                    System.out.println("Your tasks for " + schedule[1][0] + ": " + schedule[1][1]);
                    break;
                case "tuesday":
                    if (isChangeMode) {
                        schedule[2][1] = newTasks;
                    }
                    System.out.println("Your tasks for " + schedule[2][0] + ": " + schedule[2][1]);
                    break;
                case "wednesday":
                    if (isChangeMode) {
                        schedule[3][1] = newTasks;
                    }
                    System.out.println("Your tasks for " + schedule[3][0] + ": " + schedule[3][1]);
                    break;
                case "thursday":
                    if (isChangeMode) {
                        schedule[4][1] = newTasks;
                    }
                    System.out.println("Your tasks for " + schedule[4][0] + ": " + schedule[4][1]);
                    break;
                case "friday":
                    if (isChangeMode) {
                        schedule[5][1] = newTasks;
                    }
                    System.out.println("Your tasks for " + schedule[5][0] + ": " + schedule[5][1]);
                    break;
                case "saturday":
                    if (isChangeMode) {
                        schedule[6][1] = newTasks;
                    }
                    System.out.println("Your tasks for " + schedule[6][0] + ": " + schedule[6][1]);
                    break;
                case "exit":
                    status = false;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}
