package com.company;

import java.util.Random;

public class Dog extends Pet {
    public Dog(Random random, String nickName, int age, int trickLevel, String[] habits) {
        super(random, nickName, age, trickLevel, habits);
    }

    public void respond() {
        System.out.println("Dogs often chase cats");
    }
}
