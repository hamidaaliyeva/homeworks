package com.company;

import java.util.Random;

public class RoboCat extends Pet {
    public RoboCat(Random random, String nickName, int age, int trickLevel, String[] habits) {
        super(random, nickName, age, trickLevel, habits);
    }

    public void respond() {
        System.out.println("A cat robot works with battery");
    }
}
