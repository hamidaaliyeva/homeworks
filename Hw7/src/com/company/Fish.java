package com.company;

import java.util.Random;

public class Fish extends Pet {
    public Fish(Random random, String nickName, int age, int trickLevel, String[] habits) {
        super(random, nickName, age, trickLevel, habits);
    }


    public void respond() {
        System.out.println("a fish does not know about them at all");
    }
}
