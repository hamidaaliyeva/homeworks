package com.company;

import java.util.Random;

public class DomesticCat extends Pet {
    public DomesticCat(Random random, String nickName, int age, int trickLevel, String[] habits) {
        super(random, nickName, age, trickLevel, habits);
    }

    public void respond() {
        System.out.println("a domestic cat has 32 muscles that control the outer ear");
    }
}
