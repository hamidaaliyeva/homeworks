package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Family implements HumanCreator {
    private Man m;
    private Woman w;
    private Human mother;
    private Human father;
    private List<Human> children;
    private Pet pet;
    Random random = new Random();

    public void bornChild() {
        System.out.println("-");
    }


    @Override
    public void getGender() {
        List<String> men = new ArrayList<>();
        List<String> women = new ArrayList<>();
        women.add("Hamida");
        men.add("Zaur");
        women.add("Fidan");
        women.add("Cemile");
        women.add("Turkan");
        men.add("Rovshan");
        men.add("Ali");
        women.add("Nargis");
        women.add("Naila");
        men.add("Elvin");
        int number = random.nextInt(2);
        if (number == 0) {
            Man man = new Man(men.get(random.nextInt(4)), "Aliyev",
                    (mother.getIq() + father.getIq()) / 2);
            System.out.println(man);
        } else {
            Woman woman = new Woman(women.get(random.nextInt(4)), "Aliyeva",
                    (mother.getIq() + father.getIq()) / 2);
            System.out.println(woman);
        }
    }

    public Family() {
    }

    Family family;
    int index;
    Human child;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family1 = (Family) o;
        return index == family1.index
                && Objects.equals(getMother(), family1.getMother())
                && Objects.equals(getFather(), family1.getFather())
                && Objects.equals(getChildren(), family1.getChildren())
                && Objects.equals(getPet(), family1.getPet())
                && Objects.equals(family, family1.family)
                && Objects.equals(child, family1.child);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMother(), getFather(), getChildren(), getPet(), family, index, child);
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Family(Human mother, Human father, List<Human> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }


    @Override
    public String toString() {
        Boolean isCountFamily = !(family == null || countFamily(family) == 0);

        if (family != null && delete(family, child)) {
            return "Family{" +
                    "mother=" + mother +
                    ", father=" + father +
                    ", children=" + family.getChildren() +
                    ", pet=" + pet +
                    '}';
        } else if (family != null && deleteChild(family, index)) {
            return "Family{" +
                    "mother=" + mother +
                    ", father=" + father +
                    ", children=" + family.getChildren() +
                    ", pet=" + pet +
                    '}';

        } else if (family != null && addChild(family, child)) {
            return "Family{" +
                    "mother=" + mother +
                    ", father=" + father +
                    ", children=" + family.getChildren() +
                    ", pet=" + pet +
                    '}';
        } else if (isCountFamily) {
            return "Family{" +
                    "mother=" + mother +
                    ", father=" + father +
                    ", children=" + children +
                    ", size=" + countFamily(family) +
                    ", pet=" + pet +
                    '}';
        } else {
            return "Family{" +
                    "mother=" + mother +
                    ", father=" + father +
                    ", children=" + children +
                    ", pet=" + pet +
                    '}';
        }
    }

    public static boolean delete(Family family, Human child) {
        if (family.getChildren().contains(child)) {
            family.getChildren().remove(child);
            return true;
        } else return false;
    }

    public static boolean deleteChild(Family family, int index) {
        if (family.getChildren().get(index) != null) {
            family.getChildren().remove(index);
            return true;
        } else return false;
    }

    public static boolean addChild(Family family, Human child) {
        family.getChildren().add(child);
        return true;
    }

    public static int countFamily(Family family) {
        return 2 + family.getChildren().size();
    }
}
