package com.company;

import java.util.Arrays;
import java.util.Random;

public abstract class Pet {
    Random random = new Random();
    protected Species species;
    protected String nickName;
    protected int age;
    protected int trickLevel = random.nextInt(100);
    protected String[] habits;

    public Pet() {
    }

    public Pet(Random random, String nickName, int age, int trickLevel, String[] habits) {
        this.random = random;
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat() {
        System.out.println("All animals eat the same way");
    }

    abstract public void respond();

    @Override
    public String toString() {
        if (species == Species.Dog) {
            return "Pet{" +
                    "random=" + random +
                    ", species=" + Species.Dog +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + Arrays.toString(habits) +
                    '}';
        } else if (species == Species.Fish) {
            return "Pet{" +
                    "random=" + random +
                    ", species=" + Species.Fish +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + Arrays.toString(habits) +
                    '}';
        } else if (species == Species.DomesticCat) {
            return "Pet{" +
                    "random=" + random +
                    ", species=" + Species.DomesticCat +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + Arrays.toString(habits) +
                    '}';
        } else if (species == Species.RoboCat) {
            return "Pet{" +
                    "random=" + random +
                    ", species=" + Species.RoboCat +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + Arrays.toString(habits) +
                    '}';
        } else
            return "Pet{" +
                    "random=" + random +
                    ", species=" + Species.UNKNOWN +
                    ", nickName='" + nickName + '\'' +
                    ", age=" + age +
                    ", trickLevel=" + trickLevel +
                    ", habits=" + Arrays.toString(habits) +
                    '}';

    }
}
