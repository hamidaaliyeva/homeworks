package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class FamilyTest {

    Human mother = new Human("Ayten", "Aliyeva", 1985);
    Human father = new Human("Nazim", "Aliyev", 1980);
    Human girl = new Human("Hamida", "Aliyeva", 1999);
    Human boy = new Human("Rovshen", "Aliyev", 2000, mother, father);
    List<Human> children = new ArrayList<>();
    Family family = new Family(mother, father, children);

    @Test
    void deleteChild() {
        int index = 0;
        children.add(girl);
        children.add(boy);
        Assertions.assertTrue(Family.deleteChild(family, index));
    }

    @Test
    void delete() {
        children.add(girl);
        children.add(boy);
        Assertions.assertTrue(Family.delete(family, girl));
    }

    @Test
    void addChild() {
        Assertions.assertTrue(Family.addChild(family, girl));
    }

    @Test
    void countFamily() {
        children.add(girl);
        children.add(boy);
        boolean isCountFamily = !(family == null || Family.countFamily(family) == 0);
        Assertions.assertTrue(isCountFamily);
    }

    @Test
    void hashcodeFamily() {
        boolean isHashCode = !(family.hashCode() == 0);
        Assertions.assertTrue(isHashCode);
    }

    @Test
    void equalsFamily() {
        Assertions.assertTrue(family.equals(family));
    }

    Pet pet = new Pet();

    @Test
    void hashcodePet(){
        boolean isHashCode = !(pet.hashCode() == 0);
        Assertions.assertTrue(isHashCode);
    }

    @Test
    void equalsPet() {
        Assertions.assertTrue(pet.equals(pet));
    }
}