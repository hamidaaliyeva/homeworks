package com.company;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Pet {
    Random random = new Random();
    private String species;
    private String nickName;
    private int age;
    private int trickLevel = random.nextInt(100);
    private String[] habits;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;
        Pet pet = (Pet) o;
        return getAge() == pet.getAge() &&
                getTrickLevel() == pet.getTrickLevel() &&
                Objects.equals(random, pet.random) &&
                Objects.equals(getSpecies(), pet.getSpecies()) &&
                Objects.equals(getNickName(), pet.getNickName());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(random, getSpecies(), getNickName(), getAge(), getTrickLevel());
        result = 31 * result + Arrays.hashCode(getHabits());
        return result;
    }

    public void ead(Pet pet) {
        System.out.println("I am eating");
    }

    public void respond(Pet pet) {
        System.out.println("Hello, owner. I am " + pet.getNickName() + "I miss you!");
    }

    public void foul(Pet pet) {
        System.out.println("I need to cover it up");
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickName='" + nickName + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    public Pet(String nickName) {
        this.nickName = nickName;
    }

    public Pet(String species, String nickName, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {

    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }
}
