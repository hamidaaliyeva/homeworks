package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        String[] dogHabits = {"eat", "drink", "sleep"};
        Pet pet = new Pet("dog", "Rock", 5, 75, dogHabits);
        System.out.println(pet);
//        Human mother = new Human("Jane", "Karleone", 1985);
        Human mother = new Human("Aytan", "Aliyeva", 1985);
//        Human father = new Human("Vito", "Vito", 1975);
        Human father = new Human("Nazim", "Aliyev", 1975);
        Human people = new Human("Michael", "Karleone", 1977, 90, mother, father, pet);
        System.out.println(people);
        Random random = new Random();
        System.out.println(mother);
        System.out.println(father);
        Human girl = new Human("Hamida", "Aliyeva", 1999);
        System.out.println(girl);
        Human boy = new Human("Rovshen", "Aliyev", 2000, mother, father);
        System.out.println(boy);
        Pet pet1 = new Pet("Sugar");
        String[] habits = {"sleeping", "eating", "playing"};
        Pet cat = new Pet();
        int trick = cat.getTrickLevel();
        cat = new Pet("cat", "Leo", 2, trick, habits);
        Pet dog = new Pet();
        String[][] schedule = {{"weekdays"}, {"go to school"},
                {"weekends"}, {"relax"}};
        Human human = new Human();
        int iq = human.getIq();
        human = new Human("Fidan", "Abbaszada", 1998, iq, cat, mother, father, schedule);
        Human person = new Human();
        System.out.println(human);
        int generatedNumber = random.nextInt(100);
        human.feedPed(generatedNumber);
        List<Human>children = new ArrayList<>();
        children.add(girl);
        children.add(boy);
        Family family = new Family(mother, father, children);
        System.out.println(family);
        family.deleteChild(family,0);
        System.out.println(family);
        Human child = new Human("Zaur", "Qurbanli", 1998);
        family.addChild(family,child);
        System.out.println(family);
        System.out.println(family.countFamily(family));
    }
}