package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class FamilyTest {

    @Test
    void delete() {
        Family family = new Family();
        Human mother = new Human("Ayten", "Aliyeva", 90);
        Human father = new Human("Nazim", "Aliyev", 85);
        family.setMother(mother);
        family.setFather(father);
        List<Human> children = new ArrayList<>();
        Human boy = new Human("Rovshan", "Aliyev", 95);
        Human girl = new Human("Hamida", "Aliyeva", 100);
        children.add(girl);
        children.add(boy);
        family.setChildren(children);
        int childrenSizeBefore = family.getChildren().size();
        family.delete(family, girl);
        int childrenSizeAfter = family.getChildren().size();
        Assertions.assertTrue(childrenSizeBefore - childrenSizeAfter == 1);
    }

    @Test
    void deleteChild() {
        Family family = new Family();
        Human mother = new Human("Ayten", "Aliyeva", 90);
        Human father = new Human("Nazim", "Aliyev", 85);
        family.setMother(mother);
        family.setFather(father);
        List<Human> children = new ArrayList<>();
        Human boy = new Human("Rovshan", "Aliyev", 95);
        Human girl = new Human("Hamida", "Aliyeva", 100);
        children.add(girl);
        children.add(boy);
        family.setChildren(children);
        int childrenSizeBefore = family.getChildren().size();
        family.deleteChild(family, 0);
        int childrenSizeAfter = family.getChildren().size();
        Assertions.assertTrue(childrenSizeBefore - childrenSizeAfter == 1);
    }

    @Test
    void addChild() {
        Family family = new Family();
        Human mother = new Human("Ayten", "Aliyeva", 90);
        Human father = new Human("Nazim", "Aliyev", 85);
        family.setMother(mother);
        family.setFather(father);
        List<Human> children = new ArrayList<>();
        Human boy = new Human("Rovshan", "Aliyev", 95);
        Human girl = new Human("Hamida", "Aliyeva", 100);
        children.add(boy);
        family.setChildren(children);
        int childrenSizeBefore = family.getChildren().size();
        family.addChild(family, girl);
        int childrenSizeAfter = family.getChildren().size();
        Assertions.assertTrue(childrenSizeAfter - childrenSizeBefore == 1);
    }

    @Test
    void countFamily() {
        Family family = new Family();
        Human mother = new Human("Ayten", "Aliyeva", 90);
        Human father = new Human("Nazim", "Aliyev", 85);
        family.setMother(mother);
        family.setFather(father);
        List<Human> children = new ArrayList<>();
        Human boy = new Human("Rovshan", "Aliyev", 95);
        Human girl = new Human("Hamida", "Aliyeva", 100);
        children.add(girl);
        children.add(boy);
        family.setChildren(children);
        int size = family.countFamily(family);
        Assertions.assertEquals(4, size);
    }
}