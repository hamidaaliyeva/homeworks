package com.company;

import java.util.Random;
import java.util.Set;

public class Fish extends Pet {
    public Fish(String nickName, int age, int trickLevel, Set<String> habits) {
        super(nickName, age, trickLevel, habits);
    }


    public void respond() {
        System.out.println("a fish does not know about them at all");
    }
}
