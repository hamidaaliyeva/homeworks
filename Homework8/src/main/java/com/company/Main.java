package com.company;

import java.util.*;

//import static com.company.Family.randomGender;


public class Main {

    public static void main(String[] args) {
//        Pet pet = new Dog();

        String[] dogHabits = {"eat", "drink", "sleep"};
//        randomGender();
//        Pet pet = new Pet( "Rock", 5, 75, dogHabits);
//        System.out.println(pet);
        Human mother = new Human("Aytan", "Aliyeva", 90);
        Human father = new Human("Nazim", "Aliyev", 85);
//        Human people = new Human("Michael", "Karleone", 1977, 90, mother, father, pet);
//        System.out.println(people);
        Random random = new Random();
        Human girl = new Human("Hamida", "Aliyeva", 97);
//        System.out.println(girl);
        Human boy = new Human("Rovshen", "Aliyev", 79, mother, father);
//        System.out.println(boy);
        Set<String> habits = new HashSet<>();
        habits.add("sleeping");
        habits.add("eating");
        habits.add("playing");
//        Pet cat = new Pet();
//        int trick = cat.getTrickLevel();
//        cat = new Pet(Species.CAT, "Leo", 2, trick, habits);
//        Pet dog = new Pet();
        Map<String, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY.name(), "go to school");
        schedule.put(DayOfWeek.TUESDAY.name(), "go to school");
        schedule.put(DayOfWeek.WEDNESDAY.name(), "go to school");
        schedule.put(DayOfWeek.THURSDAY.name(), "go to school");
        schedule.put(DayOfWeek.FRIDAY.name(), "go to school");
        schedule.put(DayOfWeek.SATURDAY.name(), "relax");
        schedule.put(DayOfWeek.SUNDAY.name(), "do homework");
        Human human = new Human();
//        int iq = human.getIq();
//        human = new Human("Fidan", "Abbaszada", 1998, iq, cat, mother, father, schedule);
//        Human person = new Human();
//        System.out.println(human);
//        int generatedNumber = random.nextInt(100);
//        human.feedPed(generatedNumber);
        Family f = new Family();
//        f.getGender();
        List<Human> children = new ArrayList<>();
        children.add(girl);
        children.add(boy);
        Family family = new Family(mother, father, children);
        family.getGender();
        System.out.println(family);
        family.deleteChild(family, 0);
        System.out.println(family);
        Human child = new Human("Zaur", "Qurbanli", 1998);
        family.addChild(family, child);
        System.out.println(family);
        System.out.println(family.countFamily(family));
        human.bornChild();
    }
}